#include <stdlib.h>

#include "Alloc.h"
#include "stdio.h"

#ifdef _DEBUG
//#include <vld.h>
#endif

void func01()
{
	char* data1 = new char[64];
	delete[] data1;

	char* data2 = new char;
	delete data2;

	char* data3 = new char[128];
	delete data3;

	char* data[5];
	for (int i = 0; i < 5; i++) {
		data[i] = new char[60];
	}
	for (int i = 4; i >= 0; i--) {
		delete[] data[i];
	}

	for (int i = 0; i < 5; i++) {
		data[i] = new char[61];
	}
	for (int i = 0; i < 5; i++) {
		delete[] data[i];
	}

	for (int i = 0; i < 5; i++) {
		data[i] = new char[62];
	}
	for (int i = 0; i < 5; i++) {
		delete[] data[i];
	}

	//char* data01[120];
	//for (int i = 0; i < 120; i++) {
	//	data[i] = new char[1 + rand() % 1024];
	//}
	//for (int i = 120; i < 120; i++) {
	//	delete[] data[i];
	//}
}

void func02()
{
	int *data1 = (int*)mem_alloc(64);
	mem_free(data1);

	int *data2[10];
	for (int i = 0; i < 10; i++) {
		data2[i] = (int*)mem_alloc(128);
	}
	for (int i = 0; i < 10; i++) {
		mem_free(data2[i]);
	}

	int *data3 = (int*)mem_alloc(2048);
	mem_free(data3);
}

int main() 
{
	func02();

	getchar();
	return 0;
}