#include "Alloc.h"
#include <stdlib.h>
#include "MemoryMgr.hpp"

//void* operator new(size_t size){
//	return MemoryMgr::GetInstance().allocMem(size);
//}
//
//void* operator new[](size_t size){
//	return MemoryMgr::GetInstance().allocMem(size);
//}
//
//void operator delete(void* p){
//	MemoryMgr::GetInstance().freeMem(p);
//}
//
//void operator delete[](void* p){
//	MemoryMgr::GetInstance().freeMem(p);
//}

void* mem_alloc(size_t size)
{
	return MemoryMgr::GetInstance().allocMem(size);
}

void mem_free(void* p)
{
	MemoryMgr::GetInstance().freeMem(p);
}