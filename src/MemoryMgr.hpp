#ifndef _MEMORY_MGR_HPP_
#define _MEMORY_MGR_HPP_

#include <stdlib.h>
#include <mutex>
#include <assert.h>
#define MAX_MEMORY_SZIE 1024

#ifdef _DEBUG
#include <stdio.h>
#define xPrintf(...) printf(__VA_ARGS__)
#else
#define xPrintf(...)
#endif //_DEBUG

class MemoryAlloc;
//内存块 最小单元
class MemoryBlock
{	
public:
	// 内存块编号
	int nID;
	// 引用次数
	unsigned int nRef;
	// 所属大内存块(池)
	MemoryAlloc* pAlloc;
	// 下一块位置
	MemoryBlock* pNext;
	// 是否在内存池中
	bool bPool;
private:
	char c1;
	char c2;
	char c3;
};

//内存池
class MemoryAlloc{
public:
	MemoryAlloc(){
		_pBuf = NULL;
		_pHeader = NULL;
		xPrintf("MemoryAlloc\n");
	}
	~MemoryAlloc(){
		if (_pBuf){
			::free(_pBuf);
			_pBuf = NULL;
		}
	}

	void* AllocMemory(size_t nSize)
	{
		std::lock_guard<std::mutex> lg(_mutex);
		if (NULL == _pBuf){
			InitMemory();
		}
		MemoryBlock* pReturn = NULL;
		if (NULL == _pHeader){
			pReturn = (MemoryBlock*)malloc(nSize + sizeof(MemoryBlock));	// 额外申请的内存
			pReturn->bPool = false;
			pReturn->nID = -1;
			pReturn->nRef = 1;
			pReturn->pAlloc = this;
			pReturn->pNext = NULL;
		}
		else
		{
			pReturn = _pHeader;
			pReturn->bPool = true;
			_pHeader = _pHeader->pNext;
			assert(0 == pReturn->nRef);
			pReturn->nRef = 1;
			pReturn->pNext = _pHeader;
			//xPrintf("AllocMemory: _pHeader=%p\n", _pHeader);
		}
		xPrintf("AllocMemory: %p, id=%d, size=%d _nSize=%d\n", pReturn, pReturn->nID, nSize, _nSize);
		return ((char*)pReturn + sizeof(MemoryBlock));
	}

	void freeMemory(void* pMem)
	{
		std::lock_guard<std::mutex> lg(_mutex);
		MemoryBlock* pBlock = (MemoryBlock*)((char*)pMem - sizeof(MemoryBlock));
		assert(1 == pBlock->nRef);
		if (--pBlock->nRef != 0){
			return;
		}
		if (pBlock->bPool)
		{
			pBlock->pNext = _pHeader;  //被释放的块的next指向未使用的块
			_pHeader = pBlock;  //被释放的块变成未使用的块
			//xPrintf("freeMemory: _pHeader=%p\n", _pHeader);
		}
		else 
		{
			::free(pBlock);
		}
	}

	//初始化
	void InitMemory(){
		xPrintf("initMemory:_nSzie=%d,_nBlockSzie=%d\n", _nSize, _nBlockSize);
		//断言
		assert(NULL == _pBuf);
		if (NULL != _pBuf)
			return;
		//计算内存池的大小
		size_t realSzie = _nSize + sizeof(MemoryBlock);
		size_t bufSize = realSzie*_nBlockSize;
		//向系统申请池的内存
		_pBuf = (char*)malloc(bufSize);

		//初始化内存池
		_pHeader = (MemoryBlock*)_pBuf;
		_pHeader->bPool = true;
		_pHeader->nID = 0;
		_pHeader->nRef = 0;
		_pHeader->pAlloc = this;
		_pHeader->pNext = NULL;
		//遍历内存块进行初始化
		MemoryBlock* pTemp1 = _pHeader;

		for (size_t n = 1; n < _nBlockSize; n++)
		{
			MemoryBlock* pTemp2 = (MemoryBlock*)(_pBuf + (n * realSzie));
			pTemp2->bPool = true;
			pTemp2->nID = n;
			pTemp2->nRef = 0;
			pTemp2->pAlloc = this;
			pTemp2->pNext = NULL;
			pTemp1->pNext = pTemp2;
			pTemp1 = pTemp2;
		}
	}
public:
	// 内存单元大小
	unsigned int _nSize;
	// 内存单元数量
	unsigned int _nBlockSize;
private:
	// 内存池地址
	char* _pBuf;
	// 头部内存单元
	MemoryBlock* _pHeader;
	std::mutex _mutex;
};

//便于在声明类成员变量时初始化MemoryAlloc的成员数据
template<size_t nSize, size_t nBlockSzie>
class MemoryAlloctor :public MemoryAlloc
{
public:
	MemoryAlloctor()
	{
		//8 4   61/8=7  61%8=5
		const size_t n = sizeof(void*);
		//(7*8)+8 
		_nSize = (nSize / n) * n + (nSize % n ? n : 0);
		_nBlockSize = nBlockSzie;
		xPrintf("MemoryAlloctor\n");
	}
};

//内存管理工具
//进程正常结束 额外申请的内存不会自动释放
class MemoryMgr{
private:
	MemoryMgr(){
		init_szAlloc(0, 64, &_mem64);
		init_szAlloc(65, 128, &_mem128);
		init_szAlloc(129, 256, &_mem256);
		init_szAlloc(257, 512, &_mem512);
		init_szAlloc(513, 1024, &_mem1024);
	}
	~MemoryMgr()
	{
	
	}
	// 初始化内存池映射数组
	void init_szAlloc(int nBegin, int nEnd, MemoryAlloc* pMemA)
	{
		for (int n = nBegin; n <= nEnd && n <= MAX_MEMORY_SZIE; n++){
			_szAlloc[n] = pMemA;
		}
	}
public:
	static MemoryMgr& GetInstance()
	{
		static MemoryMgr mgr;
		return mgr;
	}

	void* allocMem(size_t nSize)
	{
		if (0 >= nSize) return NULL;

		if (nSize <= MAX_MEMORY_SZIE)
		{
			return _szAlloc[nSize]->AllocMemory(nSize);
		}
		else
		{
			MemoryBlock* pReturn = (MemoryBlock*)malloc(nSize + sizeof(MemoryBlock));	// 额外申请的内存
			pReturn->bPool = false;
			pReturn->nID = -1;
			pReturn->nRef = 1;
			pReturn->pAlloc = NULL;
			pReturn->pNext = NULL;
			xPrintf("allocMem: %p, id=-1, size=%d\n", pReturn, nSize);
			return (void*)((char*)pReturn + sizeof(MemoryBlock));
		}		
	}

	void freeMem(void* pMem)
	{
		if (NULL == pMem) return;

		MemoryBlock* pBlock = (MemoryBlock*)((char*)pMem - sizeof(MemoryBlock));
		xPrintf("freeMem: %p, id=%d\n", pBlock, pBlock->nID);
		if (pBlock->bPool)
		{
			pBlock->pAlloc->freeMemory(pMem);
		}
		else 
		{
			if (--pBlock->nRef == 0)
				::free(pBlock);
		}
	}
	////增加引用计数
	//void addRef(void* pMem){
	//	MemoryBlock* pBlock = (MemoryBlock*)((char*)pMem - sizeof(MemoryBlock));
	//	++pBlock->nRef;
	//}
private:
	MemoryAlloctor<64, 100000>   _mem64;
	MemoryAlloctor<128, 100000>  _mem128;
	MemoryAlloctor<256, 100000>  _mem256;
	MemoryAlloctor<512, 100000>  _mem512;
	MemoryAlloctor<1024, 100000> _mem1024;
	MemoryAlloc* _szAlloc[MAX_MEMORY_SZIE + 1];
	
};

#endif
